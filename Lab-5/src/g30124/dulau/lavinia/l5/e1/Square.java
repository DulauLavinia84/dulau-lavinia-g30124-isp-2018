package g30124.dulau.lavinia.l5.e1;

public class Square extends Rectangle{

	public Square() {
		
	}
	public Square(double side) {
		super(side,side);
	}
	
	public Square(double side, String color, boolean filled) {
		super(side,side,color,filled);
	}
	
	public double getSide() {
		return super.getWidth();
	}
	
	public void setSide(double side) {
		super.width = side;
		super.length = side;
	}
	
	@Override
	public void setWidth(double side) {
		this.width = side;
	}
	
	public void setLength(double side) {
		this.length = side;
	}
	
	public String toString() {
		return "Square(side="+getSide()+" which is a subclass of"+super.toString();
	}
	public static void main (String [] args) {
		Square s = new Square(4.0,"red",true);
		s.setSide(4.0);
		System.out.println("Area of square is: " +s.getArea());
		System.out.println("Perimeter of square is: " +s.getPerimeter());
		System.out.println("Side of square is: " +s.getSide());
		System.out.println(s.toString());
	}
}
