package g30124.dulau.lavinia.l5.e1;

public class Circle extends Shape{

	private double radius;
	
	public Circle() {
		this.radius = 18;
	}
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public Circle(double radius, String color, boolean filled) {
		super(color,filled);
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	@Override
	public double getArea() {
		return 3.14*this.radius*this.radius;
	}
	@Override
	public double getPerimeter() {
		return 2*3.14*this.radius;
	}
	@Override
	public String toString() {
		return "Circle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", radius=" + radius +
	               '}';
		
	}
	
	public static void main (String [] args) {
		Circle c = new Circle(8.0,"green",true);
		System.out.println(c.toString());
		System.out.println("Area of circle is: " +c.getArea());
		System.out.println("Perimeter of circle is: " +c.getPerimeter());
		System.out.println("Radius of circle is: " +c.getRadius());
	}
}
