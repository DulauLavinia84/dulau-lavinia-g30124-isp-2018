package g30124.dulau.lavinia.l5.e1;

public class Rectangle extends Shape{

	protected double width;
	protected double length;
	
	public Rectangle() {
		this.width = 4;
		this.length = 8;
	}
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}
	
	public Rectangle(double width, double length, String color, boolean filled) {
		super(color,filled);
		this.width = width;
		this.length = length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		this.length=length;
	}
	@Override
	public double getArea() {
		return this.width*this.length;
	}
	@Override
	public double getPerimeter() {
		return 2*this.width+2*this.length;
	}
	@Override
	public String toString() {
		return "Rectangle(width="+this.width+" length="+this.length+") which is a subclass of "+super.toString();
	}

	public static void main (String [] args) {
		Rectangle r = new Rectangle(4.0,8.0,"yellow",true);
		r.setWidth(4.0);
		r.setLength(8.0);
		System.out.println(r.toString());
		System.out.println("Area of ractangle is: " +r.getArea());
		System.out.println("Perimeter of rectangle is: " +r.getPerimeter());
		System.out.println("Width of rectangle is: " +r.getWidth());
		System.out.println("Length of rectangle is: " +r.getLength());
	}
	
}