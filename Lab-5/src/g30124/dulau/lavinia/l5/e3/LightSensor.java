package g30124.dulau.lavinia.l5.e3;

import java.util.Random;
import static java.lang.Math.abs;

public class LightSensor extends Sensor {
	
	private Random l = new Random(100);
	
	public LightSensor(String location) {
		super(location);
	}
	
	@Override
	public int readValue() {
		return abs(l.nextInt()%100);
	}

}
