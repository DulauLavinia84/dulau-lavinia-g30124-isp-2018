package g30124.dulau.lavinia.l5.e3;

import java.util.Random;
import static java.lang.Math.abs;

public class TemperatureSensor extends Sensor{
	
	private Random t = new Random(100);
	
	public TemperatureSensor(String location) {
		super(location);
	}
	
	@Override
	public int readValue() {
		return abs(t.nextInt()%100);
	}

}
