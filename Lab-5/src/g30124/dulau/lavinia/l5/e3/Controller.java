package g30124.dulau.lavinia.l5.e3;

import java.util.concurrent.TimeUnit;

public class Controller {

	private Sensor l = new LightSensor("sufragerie");
	private Sensor t = new TemperatureSensor("baie");
	
	public void display() {
		for(int i=0;i<20;i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				System.out.println("Light sensor: " +l.readValue());
				System.out.println("Temperature sensor: " +t.readValue());	
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	public static void main(String []args) {
		Controller c = new Controller();
		c.display();
	}
}
