package g30124.dulau.lavinia.l5.e2;

public class ProxyImage implements Image{
	 
	   private String fileName;
	   private Image image;
	 
	   public ProxyImage(String fileName, boolean realRotated){
	      this.fileName = fileName;
	      if(realRotated == true)
	    	  this.image = new RotatedImage(this.fileName);
	      else
	    	  this.image = new RealImage(this.fileName);
	   }
	 
	   @Override
	   public void display() {
	      image.display();
	   }
	   
	   public static void main(String[] args) {
		   ProxyImage circle = new ProxyImage("minge",true);
		   circle.display();
	   }
	}
