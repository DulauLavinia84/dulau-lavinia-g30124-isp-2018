package g30124.dulau.lavinia.l6.e4;

import java.util.Arrays;

public class Sequence implements CharSequence {

	private char[] text;

    public Sequence(char[] row) {
        this.text = row;
    }

    @Override
    public int length() {
        return text.length;
    }

    @Override
    public char charAt(int index) {
        return text[index];
    }

    @Override
    public CharSequence subSequence(int from, int to) {
        char[] word = Arrays.copyOfRange(text,from,to);
        return new Sequence(word);
    }
    
    public static void main(String[] args) {
    	String str = "Cosmina are prune";
    	char[] row = str.toCharArray();
    	Sequence newchar = new Sequence(row);
    	System.out.println("The letter from the desired position is: " +newchar.charAt(10));
    	System.out.println("The length of the string is: " +newchar.length());
    	System.out.println("The chosen substring is: " +newchar.subSequence(4,7));
    	}

}

