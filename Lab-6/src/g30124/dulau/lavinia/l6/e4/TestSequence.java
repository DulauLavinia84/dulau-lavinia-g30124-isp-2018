package g30124.dulau.lavinia.l6.e4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestSequence {

	@Test
	public void testCharAt() {
		String str1 = "	Cosmina are prune";
    	char[] sir1 = str1.toCharArray();
    	Sequence newchar1 = new Sequence(sir1);
    	String str2 = "porumbel";
    	char[] row2 = str2.toCharArray();
    	Sequence newchar2 = new Sequence(row2);
    	assertEquals(newchar1.charAt(5),newchar2.charAt(2));
	}
	
	@Test
	public void testLength() {
		String str1 = "pahar";
		char[] row1 = str1.toCharArray();
		Sequence newchar1 = new Sequence(row1);
		String str2 = "usa";
		char[] row2 = str2.toCharArray();
		Sequence newchar2 = new Sequence(row2);
		assertEquals(newchar1.length(),newchar2.length());
	}
	
	@Test
	public void testSubSequence() {
		String str = "capsuna";
		char[] row = str.toCharArray();
		Sequence newchar = new Sequence(row);
		assertTrue(newchar.subSequence(0,3) == "cap");
	}
}
