package g30124.dulau.lavinia.l6.e5;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing pyramid");
        this.setSize(400,400);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape l1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = l1;
                break;
            }
        }
//        shapes.add(l1);
        this.repaint();
    }
    
    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}


