package g30124.dulau.lavinia.l6.e5;

import java.awt.Graphics;

public class Rectangle implements Shape{

    private int length;
    private int width;
    private int a = 420;
    private int b = 60;
    private int bricks;

    public Rectangle(int length, int width,int bricks) {
        this.length = length;
        this.width = width;
        this.bricks = bricks;
    }

    @Override
    public void draw(Graphics g) {
        int k=1, ct = 0;
        while(ct < bricks && bricks - ct >= k) {
        for(int i=0;i<k;i++) {
        	g.drawRect(a-k*width/2+i*width,b+k*length, width, length);
        	ct++;
        }
        k++;
        }
        g.drawString("Number of unused bricks is: " +(bricks-ct),480,200);
        System.out.println("Number of unused bricks is: "+(bricks-ct));
    }
}
