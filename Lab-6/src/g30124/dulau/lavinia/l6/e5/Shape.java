package g30124.dulau.lavinia.l6.e5;

import java.awt.Graphics;

public interface Shape {

	void draw(Graphics g);
}

