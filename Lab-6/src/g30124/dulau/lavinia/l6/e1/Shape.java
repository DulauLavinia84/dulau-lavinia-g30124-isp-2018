package g30124.dulau.lavinia.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private String id;
    private boolean fill;
    protected int x;
    protected int y;

    public Shape(Color color,String id,boolean fill,int x,int y) {
        this.color = color;
        this.id = id;
        this.fill = fill;
        this.x = x;
        this.y = y;
    }

    public Color getColor() {
        return color;
    }
    
    public String getId() {
    	return id;
    }
    
    public boolean isFill() {
    	return fill;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
		
}

