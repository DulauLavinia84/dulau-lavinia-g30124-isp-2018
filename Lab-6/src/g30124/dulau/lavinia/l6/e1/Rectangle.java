package g30124.dulau.lavinia.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color, String id,boolean fill, int length,int width,int x, int y) {
        super(color,id,fill,x,y);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==true)
        	g.fillRect(x, y, length, width);
        else
            g.drawRect(x,y,length,width);
    }
}
