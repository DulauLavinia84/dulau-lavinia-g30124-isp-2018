package g30124.dulau.lavinia.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color,String id,boolean fill, int radius,int x, int y) {
        super(color,id,fill,x,y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==true)
        	g.fillOval(x, y,radius ,radius );
        else
            g.drawOval(x,y,radius,radius);
    }
}
