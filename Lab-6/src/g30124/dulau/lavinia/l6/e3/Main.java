package g30124.dulau.lavinia.l6.e3;

import java.awt.Color;

public class Main {

	public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,"a",true, 90,130,80);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,"b",false, 100,150,300);
        b1.addShape(s2);
       Shape s3 = new Rectangle(Color.YELLOW,"c", true, 40,60,60,70);
        b1.addShape(s3);
        Shape s4 = new Rectangle(Color.BLUE,"d", false, 80,150,50,170);
        b1.addShape(s4);
       // b1.deleteByld("a");
    }
}
