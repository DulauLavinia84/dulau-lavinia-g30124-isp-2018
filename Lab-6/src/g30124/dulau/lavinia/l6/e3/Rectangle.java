package g30124.dulau.lavinia.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape{

	private int length;
    private int width;
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Rectangle(Color color, String id,boolean fill, int length,int width,int x, int y) {
    	this.color = color;
        this.length = length;
        this.width = width;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
    }

    public Color getColor() {
    	return color;
    }
    
    public boolean isFill() {
    	return fill;
    }
    
    public String getId() {
    	return id;
    }
    
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==true)
        	g.fillRect(x, y, length, width);
        else
            g.drawRect(x,y,length,width);
    }
}

