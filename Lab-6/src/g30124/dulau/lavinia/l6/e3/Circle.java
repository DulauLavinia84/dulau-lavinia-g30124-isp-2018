package g30124.dulau.lavinia.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape {

	 private int radius;
	 private int x;
	 private int y;
	 private boolean fill;
	 private String id;
	 private Color color;


	    public Circle(Color color,String id,boolean fill, int radius,int x, int y) {
	        this.radius = radius;
	        this.color = color;
	        this.x = x;
	        this.y = y;
	        this.id = id;
	        this.fill = fill;
	    }

	    public int getRadius() {
	        return radius;
	    }
	    
	    public Color getColor() {
	    	return color;
	    }
	    
	    public String getId() {
	    	return id;
	    }
	    
	    public boolean isFill() {
	    	return fill;
	    }


	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
	        g.setColor(getColor());
	        if(isFill()==true)
	        	g.fillOval(x, y,radius ,radius );
	        else
	            g.drawOval(x,y,radius,radius);
	    }
	}
