package g30124.dulau.lavinia.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
    void draw(Graphics g);

	String getId();
	
	Color getColor();
}
