package g30124.dulau.lavinia.l8.e2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Character {

	public static void main(String[] args) throws FileNotFoundException {
		
		int character=0;
		File file = new File("E:\\Lavinia\\AnIIAC\\SemII\\Java\\Laborator-8\\src\\g30124\\dulau\\lavinia\\l8\\e2\\Letter.txt");
		Scanner lit = new Scanner(file);
		String f = lit.next();
		lit.close();
		System.out.println("Fisierul contine textul: "+f);
		Scanner g = new Scanner(System.in);
		System.out.println("Introduceti litera pe care doriti sa o numarati: ");
		char litera = g.next().charAt(0);
		g.close();
		for(int i=0;i < f.length(); i++) {
			if(f.charAt(i) == litera)
				character++;
		}
		System.out.println("Letter "+litera+" a fost gasita in fisier de "+character+" ori.");
	}
}

