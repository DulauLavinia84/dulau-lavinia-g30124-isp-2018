package g30124.dulau.lavinia.l7.e3;

import java.util.TreeSet;

public class BankTreeSet extends BankAccount{

		static TreeSet<BankAccount> acc = new TreeSet<BankAccount>();
		
		public BankTreeSet(String owner, double balance) {
			super(owner,balance);
		}
		
		public void addAccount(String owner, double balance) {
			BankTreeSet b = new BankTreeSet(owner,balance);
			acc.add(b);
		}
		
		public void printAccounts() {
			System.out.println("TreeSet-ul este sortat dupa balance: ");
			for(BankAccount i:acc)
				System.out.println(i.getBalance()+" "+i.getOwner());
			}
		
		public BankAccount getAccount(String owner) {
			for(BankAccount i:acc) {
				if(i.getOwner().equals(owner))
					System.out.println(i.getBalance()+" "+i.getOwner());
				//	return acc.get(i);
			}
			return null;
		}
		
		public void printAccounts(int min, int max) {
			for(BankAccount i:acc) {
				if(i.getBalance()>min && i.getBalance()<max)
					System.out.println("Numele contului este: "+i.getOwner()+" cu valoarea de "+i.getBalance());
			}
		}
		
		public static void main(String[] args) {
			
			BankTreeSet b = new BankTreeSet("Lavinia",155);
			b.addAccount("Lavinia",18451);
			b.addAccount("Roxana", 23083);
			b.addAccount("Alina", 15624);
			b.addAccount("Daniela", 16468);
			b.printAccounts();
			System.out.println("=========================================");
			b.getAccount("Alina");
			System.out.println("-----------------------------------------");
			b.printAccounts(15000, 23000);
		}
}

