package g30124.dulau.lavinia.l7.e4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Dictionary {

	HashMap<Word,Definition> dex = new HashMap<Word,Definition>();
	
	public void addWord(Word w, Definition d) {
		dex.put(w,d);
	}
	
	public Definition getDefinition(Word w,Definition d) {
		if(dex.containsKey(w))
			return d;
		return null;
	}
	
	public void getAllWord() {
		
	}
	
	public static void main(String[] args) {
		
		Dictionary d = new Dictionary();
		Definition def = new Definition("Is a colour");
		Word w = new Word("red");
		Word w1 = new Word("blue");
		d.addWord(w, def);
		d.getAllWord();
		System.out.println(d.getDefinition(w1,def));
	}
}
