package g30124.dulau.lavinia.l7.e4;

public class Definition {

	protected String description;
	
	public Definition(String def) {
		this.description = def;
	}
	
	public void setDescription(String def) {
		this.description = def;
	}
	
	public String getDescription() {
		return this.description;
	}
}