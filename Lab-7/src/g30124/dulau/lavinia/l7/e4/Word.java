package g30124.dulau.lavinia.l7.e4;

public class Word {

	protected String word;
	
	public Word(String w) {
		this.word = w;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public String getWord() {
		return this.word;
	}
}
