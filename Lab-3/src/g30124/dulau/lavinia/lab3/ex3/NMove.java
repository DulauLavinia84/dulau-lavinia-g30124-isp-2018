package g30124.dulau.lavinia.lab3.ex3;

import becker.robots.*;

public class NMove {
	
	public static void main(String[] args)
	{
		City la = new City();
		Robot jack = new Robot(la,1,1,Direction.NORTH);
		
		
		jack.move();
		jack.move();
		jack.move();
		jack.move();
		jack.move();
		jack.turnLeft();
		jack.turnLeft();
		jack.move();
		jack.move();
		jack.move();
		jack.move();
		jack.move();
	}

}