package g30124.dulau.lavinia.lab3.ex5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class FetchingTheNewspaper {
	
	public static void main(String[] args)
	{
		City la = new City();
		Wall blockAve0 = new Wall(la, 1, 1, Direction.WEST);
		Wall blockAve1 = new Wall(la, 2, 1, Direction.WEST);
		Wall blockAve2 = new Wall(la, 2, 1, Direction.SOUTH);
		Wall blockAve3 = new Wall(la, 1, 1, Direction.NORTH);
		Wall blockAve4 = new Wall(la, 1, 2, Direction.NORTH);
		Wall blockAve5 = new Wall(la, 1, 2, Direction.EAST);
		Wall blockAve6 = new Wall(la, 1, 2, Direction.SOUTH);
		Robot jack = new Robot(la, 1, 2, Direction.SOUTH);
		Thing paper = new Thing(la, 2, 2);
		
		
		jack.turnLeft();
		jack.turnLeft();
		jack.turnLeft();
		jack.move();
		jack.turnLeft();
		jack.move();
		jack.turnLeft();
		jack.move();
		jack.pickThing();
		jack.turnLeft();
		jack.turnLeft();
		jack.move();
		jack.turnLeft();
		jack.turnLeft();
		jack.turnLeft();
		jack.move();
		jack.turnLeft();
		jack.turnLeft();
		jack.turnLeft();
		jack.move();
		jack.turnLeft();
		jack.turnLeft();
		jack.turnLeft();
		
		
	}

}