package g30124.dulau.lavinia.lab3.ex4;

import becker.robots.*;

public class GoAroundWalls {
	
	public static void main(String[] args)
	{
		City la = new City();
		Wall blockAve0 = new Wall(la, 1, 1, Direction.WEST);
		Wall blockAve1 = new Wall(la, 2, 1, Direction.WEST);
		Wall blockAve2 = new Wall(la, 2, 1, Direction.SOUTH);
		Wall blockAve3 = new Wall(la, 2, 2, Direction.SOUTH);
		Wall blockAve4 = new Wall(la, 2, 2, Direction.EAST);
		Wall blockAve5 = new Wall(la, 1, 2, Direction.EAST);
		Wall blockAve6 = new Wall(la, 1, 1, Direction.NORTH);
		Wall blockAve7 = new Wall(la, 1, 2, Direction.NORTH);
		Robot jack = new Robot(la, 0, 2, Direction.WEST);
		
		
		jack.move();
		jack.move();
		jack.turnLeft();
		jack.move();
		jack.move();
		jack.move();
		jack.turnLeft();
		jack.move();
		jack.move();
		jack.move();
		jack.turnLeft();
		jack.move();
		jack.move();
		jack.move();
		jack.turnLeft();
		jack.move();
		
	}

}
