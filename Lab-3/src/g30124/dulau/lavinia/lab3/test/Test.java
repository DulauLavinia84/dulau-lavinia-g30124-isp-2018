package g30124.dulau.lavinia.lab3.test;

public class Test {
	
	
	 public static int solution(int[] A){

	    	int a=0,nr=0;
	    	while(a<A.length)
	    	{
	    		int sem=0;
	    		for(int b=1;b<A.length;b++)
	    		{
	    			if(A[a]==A[b])
	    				sem=1;
	    		if(sem==0)
	    		   nr = A[a];
	    		}
	    		a++;
	    	}
	    	return nr;
	    }

	    public static void main(String[] args) {
	        int[] A = new int[7];
	        A[0] = 9;  A[1] = 3;  A[2] = 9;
	        A[3] = 3;  A[4] = 9;  A[5] = 3;
	        A[6] = 7;
	        int result = solution(A);
	        System.out.println(result);
	        if(result==7)
	            System.out.println("Rezultat corect.");
	        else
	            System.out.println("Rezultat incorect.");
	    }
}

