package g30124.dulau.lavinia.l4.e4;

public class Author {

	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String Email) {
		this.email=Email;
	}
	 
	public char getGender() {
		return this.gender;
	}
	
	public String toString() {
		return new String("author-"+getName()+" ("+getGender()+") at "+getEmail());
		
	}
	
	public static void main(String[] args) {
		Author a = new Author("Ion Creanga","ion.creanga@yahoo.com",'M');
		System.out.println(a.toString());
		System.out.println(a.getName());
		System.out.println(a.getEmail());
		System.out.println(a.getGender());
	}
}

