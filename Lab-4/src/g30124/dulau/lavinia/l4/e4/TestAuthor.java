package g30124.dulau.lavinia.l4.e4;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestAuthor {

	@Test
	public void TestName() {
		Author a = new Author("Ion Creanga","ion.creanga@yahoo.com",'M');
		assertTrue(a.getName()=="Ion Creanga");
		
	}
	
	@Test
	public void TestEmail() {
		Author a = new Author("Ion Creanga","ion.creanga@yahoo.com",'M');
		assertTrue(a.getEmail()=="ion.creanga@yahoo.com");
		
	}
	
	@Test
	public void TestGender() {
		Author a = new Author("Ion Creanga","ion.creanga@yahoo.com",'M');
		assertTrue(a.getGender()=='M');
	}
}
