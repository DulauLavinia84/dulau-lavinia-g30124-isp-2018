package g30124.dulau.lavinia.l4.e3;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestCircle {

	@Test
	public void TestRadius() {
		Circle c = new Circle();
		assertTrue(c.getRadius()==8.0);
	}
	
	@Test
	public void TestArea() {
		Circle c = new Circle();
		assertTrue(c.getArea()==200.96);
	}
	
}
