package g30124.dulau.lavinia.l4.e3;

public class Circle {
	
	protected double radius=1.0;
	private String color="red";
	
	public Circle() {
		this.radius=8.0;
		this.color="pink";
	}
	
	public Circle(double radius) {
		this.radius=radius;
	}

	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return 3.14*this.radius*this.radius;	
	}
	
	public static void main(String[] args) {
		Circle c = new Circle();
		System.out.println("Area of circle is: " +c.getArea());
		System.out.println("Radius of circle is: " +c.getRadius());
	}
}
