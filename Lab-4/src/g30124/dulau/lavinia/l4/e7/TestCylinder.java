package g30124.dulau.lavinia.l4.e7;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestCylinder {

	@Test
	public void TestVolume() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getVolume() == 226.08);
	}
	
	@Test
	public void TestHeight() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getHeight() == 9.0);
	}
	
	@Test
	public void TestRadius() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getRadius() == 2.0);
	}
}
